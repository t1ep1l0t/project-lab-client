import { fileURLToPath } from "node:url";

export default defineNuxtConfig({
  devtools: { enabled: false },
  app: {
    head: {
      charset: "utf-8",
      viewport: "width=device-width, initial-scale=1",
      htmlAttrs: {
        lang: "en-US",
      },
      title: "ProjectLab",
    },
    pageTransition: { name: "page", mode: "out-in" },
  },
  css: ["@/assets/styles/main.scss"],
  modules: [
    "@pinia/nuxt",
    "@nuxtjs/i18n",
    "@formkit/auto-animate/nuxt",
    "@vueuse/nuxt",
    "@nuxt/eslint",
    "@nuxt/image",
    "@nuxtjs/color-mode",
    "@nuxtjs/device",
    "@nuxtjs/google-fonts",
    "@element-plus/nuxt",
  ],
  i18n: {
    vueI18n: "./config/i18n.config.ts",
    locales: ["en"],
    defaultLocale: "en",
    strategy: "prefix",
  },
  image: {
    quality: 80,
    format: ["webp"],
    densities: [1, 2],
    domains: ["localhost:3000"],
    alias: {
      localhost: "http://localhost:3000",
    },
  },
  googleFonts: {
    families: {
      Inter: [300, 400, 500, 600, 700, 900],
      "Source Code Pro": [300, 400, 500, 600],
    },
  },
  elementPlus: {
    namespace: "ui",
  },
  alias: {
    modules: fileURLToPath(new URL("./assets/styles/modules", import.meta.url)),
    global: fileURLToPath(new URL("./assets/styles/global", import.meta.url)),
    images: fileURLToPath(new URL("./assets/images", import.meta.url)),
    icons: fileURLToPath(new URL("./assets/icons", import.meta.url)),
  },
});
